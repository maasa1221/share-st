import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InfoIcon from "@material-ui/icons/Info";
import ContactIcon from "@material-ui/icons/ContactSupport";
import MailIcon from "@material-ui/icons/Mail";
import EditIcon from "@material-ui/icons/Edit";
import LockIcon from "@material-ui/icons/Lock";
import ShopIcon from "@material-ui/icons/ShoppingCart";
import styled from "styled-components";

function Sidebar(props) {
  return (
    <div>
      <Barcontena>
        <nav aria-label="mailbox folders">
          <List>
            <ListItem button>
              <ListItemIcon>
                <EditIcon />
              </ListItemIcon>
              <ListItemText primary="ãã¤ãã¼ã¸ç·¨é" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <LockIcon />
              </ListItemIcon>
              <ListItemText primary="ã»ã­ã¥ãªãã£è¨­å®" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <ContactIcon />
              </ListItemIcon>
              <ListItemText primary="ããããè³ªå" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <ShopIcon />
              </ListItemIcon>
              <ListItemText primary="æéãã©ã³ã®å¤æ´" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <InfoIcon />
              </ListItemIcon>
              <ListItemText primary="ç¹å®åå¼åæ³ã«ã¤ãã¦" />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <MailIcon />
              </ListItemIcon>
              <ListItemText primary="ãåãåãã" />
            </ListItem>
          </List>
        </nav>
      </Barcontena>
    </div>
  );
}

const Barcontena = styled.div`
  width: 20%;
`;

export default Sidebar;
